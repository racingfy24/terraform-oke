data oci_identity_regions regions {
}

data oci_identity_tenancy tenancy {
  tenancy_id = var.tenancy_ocid
}

locals {
  region_map = {
    for r in data.oci_identity_regions.regions.regions :
    r.key => r.name
  }

  home_region = lookup(
    local.region_map,
    data.oci_identity_tenancy.tenancy.home_region_key
  )
}


resource "oci_identity_dynamic_group" "this" {
   provider = oci.home
   compartment_id = var.tenancy_ocid
   name           = var.dynamic-group
   description    = "dynamic group OKE created by terraform"
   matching_rule  = "instance.compartment.id = '${var.compartment_ocid}'"
 }

resource "oci_identity_policy" "this" {
   provider = oci.home
   depends_on     = [oci_identity_dynamic_group.this]
   name           = "OKE-Policy"
   description    = "OKE Policy"
   compartment_id = var.tenancy_ocid
   statements     = var.policy-statements
 }

resource oci_core_nat_gateway export_nat-gateway-0 {
  block_traffic  = "false"
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  display_name = "${var.network_prefix}-nat-gateway-0"
  freeform_tags = {
  }
  #public_ip_id = <<Optional value not found in discovery>>
  #route_table_id = <<Optional value not found in discovery>>
  vcn_id = oci_core_vcn.export_OKE_VCN.id
}

resource oci_core_internet_gateway export_internet-gateway-0 {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  display_name = "${var.network_prefix}-internet-gateway-0"
  enabled      = "true"
  freeform_tags = {
  }
  #route_table_id = <<Optional value not found in discovery>>
  vcn_id = oci_core_vcn.export_OKE_VCN.id
}

resource oci_core_vcn export_OKE_VCN {
  #byoipv6cidr_details = <<Optional value not found in discovery>>
  cidr_block = var.kubernetes_vcn_cidr

  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  display_name = "${var.network_prefix}_VCN"
  dns_label    = "${var.dns_label_vcn}"
  freeform_tags = {
  }
  ipv6private_cidr_blocks = [
  ]
  #is_ipv6enabled = <<Optional value not found in discovery>>
  #is_oracle_gua_allocation_enabled = <<Optional value not found in discovery>>
}

resource oci_core_subnet export_KubernetesAPIendpoint-subnet {
  #availability_domain = <<Optional value not found in discovery>>
  cidr_block     = var.kubernetes_api_endpoint_cidr
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  dhcp_options_id = oci_core_vcn.export_OKE_VCN.default_dhcp_options_id
  display_name = "${var.network_prefix}-KubernetesAPIendpoint_subnet"
  dns_label    = "${var.dns_label_api}"
  freeform_tags = {
  }
  #ipv6cidr_block = <<Optional value not found in discovery>>
  ipv6cidr_blocks = [
  ]
  prohibit_internet_ingress  = "true"
  prohibit_public_ip_on_vnic = "true"
  route_table_id             = oci_core_route_table.export_routetable-KubernetesAPIendpoint.id
  security_list_ids = [
    oci_core_security_list.export_seclist-KubernetesAPIendpoint.id,
  ]
  vcn_id = oci_core_vcn.export_OKE_VCN.id
}

resource oci_core_subnet export_workernodes-subnet {
  #availability_domain = <<Optional value not found in discovery>>
  cidr_block     = var.workernodes_subnet_cidr
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  dhcp_options_id = oci_core_vcn.export_OKE_VCN.default_dhcp_options_id
  display_name = "${var.network_prefix}-workernodes-subnet"
  dns_label    = "${var.dns_label_wn}"
  freeform_tags = {
  }
  #ipv6cidr_block = <<Optional value not found in discovery>>
  ipv6cidr_blocks = [
  ]
  prohibit_internet_ingress  = "true"
  prohibit_public_ip_on_vnic = "true"
  route_table_id             = oci_core_route_table.export_routetable-pods.id
  security_list_ids = [
    oci_core_security_list.export_seclist-workernodes.id,
  ]
  vcn_id = oci_core_vcn.export_OKE_VCN.id
}

resource oci_core_subnet export_pods-subnet {
  #availability_domain = <<Optional value not found in discovery>>
  cidr_block     = var.pods_subnet_cidr
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  dhcp_options_id = oci_core_vcn.export_OKE_VCN.default_dhcp_options_id
  display_name = "${var.network_prefix}-pods-subnet"
  dns_label    = "${var.dns_label_pods}"
  freeform_tags = {
  }
  #ipv6cidr_block = <<Optional value not found in discovery>>
  ipv6cidr_blocks = [
  ]
  prohibit_internet_ingress  = "true"
  prohibit_public_ip_on_vnic = "true"
  route_table_id             = oci_core_route_table.export_routetable-pods.id
  security_list_ids = [
    oci_core_security_list.export_seclist-pods.id,
  ]
  vcn_id = oci_core_vcn.export_OKE_VCN.id
}

resource oci_core_subnet export_load-balancer-subnet {
  #availability_domain = <<Optional value not found in discovery>>
  cidr_block     = var.load_balancer_subnet
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  dhcp_options_id = oci_core_vcn.export_OKE_VCN.default_dhcp_options_id
  display_name = "${var.network_prefix}-load-balancer-subnet"
  dns_label    = "${var.dns_label_lb}"
  freeform_tags = {
  }
  #ipv6cidr_block = <<Optional value not found in discovery>>
  ipv6cidr_blocks = [
  ]
  prohibit_internet_ingress  = "false"
  prohibit_public_ip_on_vnic = "false"
  route_table_id             = oci_core_route_table.export_routetable-serviceloadbalancers.id
  security_list_ids = [
    oci_core_security_list.export_seclist-loadbalancers.id,
  ]
  vcn_id = oci_core_vcn.export_OKE_VCN.id
}

resource oci_core_default_dhcp_options export_Default-DHCP-Options-for-OKE_VCN {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  display_name     = "Default DHCP Options for VCN"
  domain_name_type = "CUSTOM_DOMAIN"
  freeform_tags = {
  }
  manage_default_resource_id = oci_core_vcn.export_OKE_VCN.default_dhcp_options_id
  options {
    custom_dns_servers = [
    ]
    #search_domain_names = <<Optional value not found in discovery>>
    server_type = "VcnLocalPlusInternet"
    type        = "DomainNameServer"
  }
  options {
    #custom_dns_servers = <<Optional value not found in discovery>>
    search_domain_names = [
      "okevcn.oraclevcn.com",
    ]
    #server_type = <<Optional value not found in discovery>>
    type = "SearchDomain"
  }
}


#######################
# Service Gateway (SGW)
#######################
data "oci_core_services" "all_oci_services" {
  filter {
    name   = "name"
    values = ["All .* Services In Oracle Services Network"]
    regex  = true
  }
  count = 1
}

resource oci_core_service_gateway export_service-gateway-0 {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  display_name = "${var.network_prefix}-service-gateway-0"

  freeform_tags = {
  }
  services {
	service_id = data.oci_core_services.all_oci_services[0].services[0].id
  }
  
  vcn_id = oci_core_vcn.export_OKE_VCN.id
}

resource oci_core_route_table export_routetable-KubernetesAPIendpoint {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  display_name = "routetable-KubernetesAPIendpoint"
  freeform_tags = {
  }
  route_rules {
    #description = <<Optional value not found in discovery>>
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = oci_core_nat_gateway.export_nat-gateway-0.id
    #route_type = <<Optional value not found in discovery>>
  }
  route_rules {
    #description = <<Optional value not found in discovery>>
    destination       = data.oci_core_services.all_oci_services[0].services[0].cidr_block
    destination_type  = "SERVICE_CIDR_BLOCK"
    network_entity_id = oci_core_service_gateway.export_service-gateway-0.id
    #route_type = <<Optional value not found in discovery>>
  }
  vcn_id = oci_core_vcn.export_OKE_VCN.id
}

resource oci_core_route_table export_routetable-pods {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  display_name = "routetable-pods"
  freeform_tags = {
  }
  route_rules {
    #description = <<Optional value not found in discovery>>
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = oci_core_nat_gateway.export_nat-gateway-0.id
    #route_type = <<Optional value not found in discovery>>
  }
  route_rules {
    #description = <<Optional value not found in discovery>>
    destination       = data.oci_core_services.all_oci_services[0].services[0].cidr_block
    destination_type  = "SERVICE_CIDR_BLOCK"
    network_entity_id = oci_core_service_gateway.export_service-gateway-0.id
    #route_type = <<Optional value not found in discovery>>
  }
  vcn_id = oci_core_vcn.export_OKE_VCN.id
}

resource oci_core_route_table export_routetable-serviceloadbalancers {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  display_name = "routetable-serviceloadbalancers"
  freeform_tags = {
  }
  route_rules {
    #description = <<Optional value not found in discovery>>
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = oci_core_internet_gateway.export_internet-gateway-0.id
    #route_type = <<Optional value not found in discovery>>
  }
  vcn_id = oci_core_vcn.export_OKE_VCN.id
}

resource oci_core_default_route_table export_Default-Route-Table-for-OKE_VCN {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  display_name = "Default Route Table for VCN"
  freeform_tags = {
  }
  manage_default_resource_id = oci_core_vcn.export_OKE_VCN.default_route_table_id
}

resource oci_core_security_list export_seclist-KubernetesAPIendpoint {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  display_name = "seclist-KubernetesAPIendpoint"
  egress_security_rules {
    description      = "Allow Kubernetes API endpoint to communicate with OKE."
    destination      = data.oci_core_services.all_oci_services[0].services[0].cidr_block
    destination_type = "SERVICE_CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "Path Discovery"
    destination      = data.oci_core_services.all_oci_services[0].services[0].cidr_block
    destination_type = "SERVICE_CIDR_BLOCK"
    icmp_options {
      code = "4"
      type = "3"
    }
    protocol  = "1"
    stateless = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "Allow Kubernetes API endpoint to communicate with worker nodes."
    destination      = var.workernodes_subnet_cidr
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    tcp_options {
      max = "10250"
      min = "10250"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "Path Discovery."
    destination      = var.workernodes_subnet_cidr
    destination_type = "CIDR_BLOCK"
    icmp_options {
      code = "4"
      type = "3"
    }
    protocol  = "1"
    stateless = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "Allow Kubernetes API endpoint to communicate with pods."
    destination      = var.pods_subnet_cidr
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "all"
    stateless = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    #description = <<Optional value not found in discovery>>
    destination      = var.kubernetes_api_endpoint_cidr
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    tcp_options {
      max = "6443"
      min = "6443"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    #description = <<Optional value not found in discovery>>
    destination      = "0.0.0.0/0"
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    tcp_options {
      max = "80"
      min = "80"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    #description = <<Optional value not found in discovery>>
    destination      = "0.0.0.0/0"
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    tcp_options {
      max = "443"
      min = "443"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  freeform_tags = {
  }
  ingress_security_rules {
    description = "Kubernetes worker to Kubernetes API endpoint communication."
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = var.workernodes_subnet_cidr
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "6443"
      min = "6443"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "Kubernetes worker to Kubernetes API endpoint communication."
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = var.workernodes_subnet_cidr
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "12250"
      min = "12250"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "Path Discovery."
    icmp_options {
      code = "4"
      type = "3"
    }
    protocol    = "1"
    source      = var.workernodes_subnet_cidr
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = " \tPod to Kubernetes API endpoint communication (when using VCN-native pod networking)."
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = var.pods_subnet_cidr
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "6443"
      min = "6443"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = " \tPod to Kubernetes API endpoint communication (when using VCN-native pod networking)."
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = var.pods_subnet_cidr
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "12250"
      min = "12250"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = var.kubernetes_api_endpoint_cidr
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "6443"
      min = "6443"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  vcn_id = oci_core_vcn.export_OKE_VCN.id
}

resource oci_core_security_list export_seclist-workernodes {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  display_name = "seclist-workernodes"
  egress_security_rules {
    description      = "Allow worker nodes to access pods."
    destination      = var.pods_subnet_cidr
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "all"
    stateless = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "Path Discovery."
    destination      = "0.0.0.0/0"
    destination_type = "CIDR_BLOCK"
    icmp_options {
      code = "4"
      type = "3"
    }
    protocol  = "1"
    stateless = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "Allow worker nodes to communicate with OKE."
    destination      = data.oci_core_services.all_oci_services[0].services[0].cidr_block
    destination_type = "SERVICE_CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "Kubernetes worker to Kubernetes API endpoint communication."
    destination      = var.kubernetes_api_endpoint_cidr
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    tcp_options {
      max = "6443"
      min = "6443"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "Kubernetes worker to Kubernetes API endpoint communication."
    destination      = var.kubernetes_api_endpoint_cidr
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    tcp_options {
      max = "12250"
      min = "12250"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "allow to pull images"
    destination      = "0.0.0.0/0"
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    tcp_options {
      max = "443"
      min = "443"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  freeform_tags = {
  }
  ingress_security_rules {
    description = "Allow Kubernetes API endpoint to communicate with worker nodes."
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = var.kubernetes_api_endpoint_cidr
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "10250"
      min = "10250"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "Path Discovery."
    icmp_options {
      code = "4"
      type = "3"
    }
    protocol    = "1"
    source      = "0.0.0.0/0"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "Load balancer to worker nodes node ports."
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = var.load_balancer_subnet
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "32767"
      min = "30000"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "Load balancer to worker nodes node ports."
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "17"
    source      = var.load_balancer_subnet
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    udp_options {
      max = "32767"
      min = "30000"
      #source_port_range = <<Optional value not found in discovery>>
    }
  }
  ingress_security_rules {
    description = "Allow load balancer to communicate with kube-proxy on worker nodes."
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = var.load_balancer_subnet
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "10256"
      min = "10256"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "Allow load balancer to communicate with kube-proxy on worker nodes."
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "17"
    source      = var.load_balancer_subnet
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    udp_options {
      max = "10256"
      min = "10256"
      #source_port_range = <<Optional value not found in discovery>>
    }
  }
  vcn_id = oci_core_vcn.export_OKE_VCN.id
}

resource oci_core_security_list export_seclist-pods {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  display_name = "seclist-pods"
  egress_security_rules {
    description      = "Allow pods to communicate with other pods."
    destination      = var.pods_subnet_cidr
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "all"
    stateless = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "Path Discovery."
    destination      = data.oci_core_services.all_oci_services[0].services[0].cidr_block
    destination_type = "SERVICE_CIDR_BLOCK"
    icmp_options {
      code = "2"
      type = "3"
    }
    protocol  = "1"
    stateless = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "Allow pods to communicate with OCI services."
    destination      = data.oci_core_services.all_oci_services[0].services[0].cidr_block
    destination_type = "SERVICE_CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "(optional) Allow pods to communicate with internet."
    destination      = "0.0.0.0/0"
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    tcp_options {
      max = "443"
      min = "443"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "Pod to Kubernetes API endpoint communication (when using VCN-native pod networking)."
    destination      = var.kubernetes_api_endpoint_cidr
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    tcp_options {
      max = "6443"
      min = "6443"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "Pod to Kubernetes API endpoint communication (when using VCN-native pod networking)."
    destination      = var.kubernetes_api_endpoint_cidr
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    tcp_options {
      max = "12250"
      min = "12250"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "(optional) Allow pods to communicate with internet."
    destination      = "0.0.0.0/0"
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    tcp_options {
      max = "80"
      min = "80"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  freeform_tags = {
  }
  ingress_security_rules {
    description = "Allow worker nodes to access pods."
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "all"
    source      = var.workernodes_subnet_cidr
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "Allow Kubernetes API endpoint to communicate with pods."
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "all"
    source      = var.kubernetes_api_endpoint_cidr
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "Allow pods to communicate with other pods."
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "all"
    source      = var.pods_subnet_cidr
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  vcn_id = oci_core_vcn.export_OKE_VCN.id
}

resource oci_core_security_list export_seclist-loadbalancers {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  display_name = "seclist-loadbalancers"
  egress_security_rules {
    description      = "Load balancer to worker nodes node ports."
    destination      = var.workernodes_subnet_cidr
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    tcp_options {
      max = "32767"
      min = "30000"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "Load balancer to worker nodes node ports."
    destination      = var.workernodes_subnet_cidr
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "17"
    stateless = "false"
    #tcp_options = <<Optional value not found in discovery>>
    udp_options {
      max = "32767"
      min = "30000"
      #source_port_range = <<Optional value not found in discovery>>
    }
  }
  egress_security_rules {
    description      = "Allow load balancer to communicate with kube-proxy on worker nodes."
    destination      = var.workernodes_subnet_cidr
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    tcp_options {
      max = "10256"
      min = "10256"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    description      = "Allow load balancer to communicate with kube-proxy on worker nodes."
    destination      = var.workernodes_subnet_cidr
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "17"
    stateless = "false"
    #tcp_options = <<Optional value not found in discovery>>
    udp_options {
      max = "10256"
      min = "10256"
      #source_port_range = <<Optional value not found in discovery>>
    }
  }
  freeform_tags = {
  }
  ingress_security_rules {
    description = "(optional) Load balancer listener protocol and port. Customize as required. "
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "0.0.0.0/0"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "443"
      min = "443"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "(optional) Load balancer listener protocol and port. Customize as required. "
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "0.0.0.0/0"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "8080"
      min = "8080"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "(optional) Load balancer listener protocol and port. Customize as required. "
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "0.0.0.0/0"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "80"
      min = "80"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  vcn_id = oci_core_vcn.export_OKE_VCN.id
}

resource oci_core_default_security_list export_Default-Security-List-for-OKE_VCN {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = var.created_by
    "Oracle-Tags.CreatedOn" = "${timestamp()}"
  }
  display_name = "Default Security List for OKE_VCN"
  egress_security_rules {
    #description = <<Optional value not found in discovery>>
    destination      = "0.0.0.0/0"
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "all"
    stateless = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  egress_security_rules {
    #description = <<Optional value not found in discovery>>
    destination      = var.workernodes_subnet_cidr
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    tcp_options {
      max = "10256"
      min = "10256"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  freeform_tags = {
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "0.0.0.0/0"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "22"
      min = "22"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    icmp_options {
      code = "4"
      type = "3"
    }
    protocol    = "1"
    source      = "0.0.0.0/0"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    icmp_options {
      code = "-1"
      type = "3"
    }
    protocol    = "1"
    source      = var.kubernetes_vcn_cidr
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "0.0.0.0/0"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "80"
      min = "80"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  manage_default_resource_id = oci_core_vcn.export_OKE_VCN.default_security_list_id
}
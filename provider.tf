terraform {
	required_version = ">= 1.0"
	required_providers {
		oci = {
			source  = "oracle/oci"
		}
	}
}

# default provider for the configuration
provider "oci" {
	region = var.region
}

# provider for home region for IAM resource provisioning
provider "oci" {
	alias            = "home"
	tenancy_ocid     = var.tenancy_ocid
	user_ocid        = var.user_ocid
	fingerprint      = var.fingerprint
	private_key_path = var.private_key_path
	region = local.home_region
}

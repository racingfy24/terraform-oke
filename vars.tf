### Geography ###
variable region {}

### Credentials ###
variable tenancy_ocid {}
variable user_ocid {default = ""}
variable fingerprint {default = ""}
variable private_key_path {default = ""}

variable compartment_ocid {}

variable network_compartment_ocid {default = ""}

#Network configuration
variable kubernetes_vcn_cidr {default = "10.0.0.0/16"}
variable kubernetes_api_endpoint_cidr {default = "10.0.0.0/29"}
variable workernodes_subnet_cidr {default = "10.0.1.0/24"}
variable pods_subnet_cidr {default = "10.0.4.0/24"}
variable load_balancer_subnet {default = "10.0.2.0/24"}
variable network_prefix {default = "RACING"}
variable dns_label_vcn {default = "oke"}
variable dns_label_api {default = "okeapi"}
variable dns_label_lb {default = "okelb"}
variable dns_label_pods {default = "okepod"}
variable dns_label_wn {default = "okewn"}

# oke cluster options 
variable "oke_cluster_name" {
  default = "OKE-RACING"
}

variable "node_shape" {
	default = "VM.Standard.E3.Flex"
}
variable "created_by" {
	default = "Oracle RedBull Racing FY24"
}

variable node_pool_size {default = 2}

variable "kubernetes_version" {default = "v1.26.2"}


#oke policies
variable "dynamic-group" {
	default = "OKE-DynamicGroup"
}

variable "policy-statements" {
  type    = list
  default = ["Allow dynamic-group OKE-DynamicGroup to manage all-resources in tenancy"]
}


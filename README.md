# terraform-oke



## Cluster with OCI CNI Plugin, Private Kubernetes API Endpoint, Private Worker Nodes, and Public Load Balancers

This example assumes you want only load balancers accessible directly from the internet. The Kubernetes API endpoint and the worker nodes are accessible within the VCN.


![Alt text](images/conteng-network-oci-cni-eg2-1.png)


You can use OCI resource manager or terraform CLI

If you are going to use terraform CLI you need:

Install terraform
Generate API keys
Obtain the necessary OCIDs (tenant, compartment and user)





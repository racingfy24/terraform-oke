module "oci-oke" {
  count                                                                       = 1
  source                                                                      = "github.com/oracle-devrel/terraform-oci-arch-oke"
  tenancy_ocid                                                                = var.tenancy_ocid
  compartment_ocid                                                            = var.compartment_ocid
  oke_cluster_name                                                            = var.oke_cluster_name
  
  #services_cidr                                                               = lookup(var.network_cidrs, "KUBERNETES-SERVICE-CIDR")
  #pods_cidr                                                                   = var.pods_subnet_cidr
  
  #cluster_options_add_ons_is_kubernetes_dashboard_enabled                     = var.cluster_options_add_ons_is_kubernetes_dashboard_enabled
  
  cluster_options_add_ons_is_tiller_enabled                                   = false
  cluster_options_admission_controller_options_is_pod_security_policy_enabled = "false"
  pool_name                                                                   = "pool1"
  node_shape                                                                  = var.node_shape
  node_ocpus                                                                  = "1"
  node_memory                                                                 = "16"
  node_count                                                                  = var.node_pool_size
  
  #node_pool_boot_volume_size_in_gbs                                           = var.node_pool_boot_volume_size_in_gbs
  
  k8s_version                                                                 = var.kubernetes_version
  use_existing_vcn                                                            = true
  vcn_id                                                                      = oci_core_vcn.export_OKE_VCN.id
  is_api_endpoint_subnet_public                                               = false
  api_endpoint_subnet_id                                                      = oci_core_subnet.export_KubernetesAPIendpoint-subnet.id
  api_endpoint_nsg_ids                                                        = []  
  is_lb_subnet_public                                                         = true
  lb_subnet_id                                                                = oci_core_subnet.export_load-balancer-subnet.id
  is_nodepool_subnet_public                                                   = false
  nodepool_subnet_id                                                          = oci_core_subnet.export_pods-subnet.id
  ssh_public_key                                                              = ""
  #defined_tags                                                                = { "${oci_identity_tag_namespace.ArchitectureCenterTagNamespace.name}.${oci_identity_tag.ArchitectureCenterTag.name}" = var.release }
}

